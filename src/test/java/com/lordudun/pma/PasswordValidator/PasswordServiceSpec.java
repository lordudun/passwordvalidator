package com.lordudun.pma.passwordvalidator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;

import org.junit.Test;

public class PasswordServiceSpec {

	@Test
	public void comprobarUnaSolaContraseľaValida() {
		
		PasswordService passwordService = new PasswordService();
		File fichero = mock(File.class);
		
		when(fichero.toString()).thenReturn("ab1C");
		
		assertEquals("ab1C", passwordService.procesaFichero(fichero));
	}
	
	@Test
	public void comprobarUnContraseľaValidaYUnaNoValida() {
		
		PasswordService passwordService = new PasswordService();
		File fichero = mock(File.class);
		
		when(fichero.toString()).thenReturn("ab1C,aaaaa");
		
		assertEquals("ab1C", passwordService.procesaFichero(fichero));
	}
}
