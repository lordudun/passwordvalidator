package com.lordudun.pma.passwordvalidator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class PasswordValidatorSpec {
	
	PasswordValidator passwordValidator;
	
	@Before
	public void prepararTest(){
		
		passwordValidator = new PasswordValidator();
	}
	
	@Test
	public void password_es_de_longitud_adecuada() {
		
		assertTrue(passwordValidator.isValidPassword("11aB"));
	}
	
	@Test
	public void password_es_de_longitud_menos_a_la_adecuada() {
		
		assertFalse(passwordValidator.isValidPassword("1aB"));
	}
	
	@Test
	public void password_no_contiene_un_numero() {
		
		assertFalse(passwordValidator.isValidPassword("yyaB"));
	}
	
	@Test
	public void password_no_contiene_una_letra_mayuscula() {
		
		assertFalse(passwordValidator.isValidPassword("yya1"));
	}
}
