package com.lordudun.pma.passwordvalidator;

public class ReglaAlgunCaracterQueEsUnaLetraMayuscula implements Regla {

	@Override
	public boolean esValida(String password) {
		
		return password.matches(".*[A-Z].*");
	}
}
