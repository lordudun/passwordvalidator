package com.lordudun.pma.passwordvalidator;

import java.util.ArrayList;
import java.util.List;

public class ReglasDeValidacion {
	
	private List<Regla> reglas;
	
	public ReglasDeValidacion() {
		
		reglas = new ArrayList<Regla>();
	}
	
	public void anadirRegla(Regla reglaAnadir) {
		
		reglas.add(reglaAnadir);
	}
	
	public boolean validaReglas(String password) {
		
		boolean satisfaceTodasLasReglas = true;
		
		for (Regla reglaAValidar : reglas) {
			
			satisfaceTodasLasReglas = satisfaceTodasLasReglas 
					&& reglaAValidar.esValida(password);
		}
		
		return satisfaceTodasLasReglas;
	}
}
