package com.lordudun.pma.passwordvalidator;

public class ReglaLongitudAdecuada implements Regla {

	@Override
	public boolean esValida(String password) {
		
		return password.length() >= 4;
	}

}
