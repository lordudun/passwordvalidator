package com.lordudun.pma.passwordvalidator;

import java.io.File;

public class PasswordService {

	private PasswordValidator passwordValidator;
	
	public PasswordService() {
	
		passwordValidator = new PasswordValidator();
	}
	
	public String procesaFichero(File fichero) {
		
		String[] passwordsAValidar = obtenerListaDePasswords(fichero.toString());
		
		return obtenerPasswordsCorrectos(passwordsAValidar);
	}
	
	private String[] obtenerListaDePasswords(String cadenaDePasswords) {
		
		return cadenaDePasswords.split(",");
	}
	
	private String obtenerPasswordsCorrectos(String[] passwordsAValidar) {
		
		String passwordsCorrectos = "";
		
		for (String passwordAValidar : passwordsAValidar) {
			
			if (passwordValidator.isValidPassword(passwordAValidar)) {
				
				passwordsCorrectos += passwordAValidar;
			}
			
		}
		
		return passwordsCorrectos;
	}
}
