package com.lordudun.pma.passwordvalidator;

public class ReglaAlgunCaracterNumerico implements Regla {

	@Override
	public boolean esValida(String password) {
		
		return password.matches(".*[1-9].*");
	}
}
