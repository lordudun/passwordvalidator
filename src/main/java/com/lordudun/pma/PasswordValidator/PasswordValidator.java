package com.lordudun.pma.passwordvalidator;

public class PasswordValidator {

	public boolean isValidPassword(String password){
		
		ReglasDeValidacion reglasDeValidacion = new ReglasDeValidacion();
		
		reglasDeValidacion.anadirRegla(new ReglaLongitudAdecuada());
		reglasDeValidacion.anadirRegla(new ReglaAlgunCaracterNumerico());
		reglasDeValidacion.anadirRegla(new ReglaAlgunCaracterQueEsUnaLetraMayuscula());
		
		return reglasDeValidacion.validaReglas(password);
	}
}
